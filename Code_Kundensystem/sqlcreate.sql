-- Drop table if it exists
DROP TABLE IF EXISTS tbl_customer;

-- Create table
CREATE TABLE tbl_customer (
  id_customer INT PRIMARY KEY,
  firstName VARCHAR(255),
  lastName VARCHAR(255),
  email VARCHAR(255)
);

-- Add records
INSERT INTO tbl_customer (id_customer, firstName, lastName, email)
VALUES
  (1, 'John', 'Doe', 'john.doe@example.com'),
  (2, 'Jane', 'Smith', 'jane.smith@example.com'),
  (3, 'Michael', 'Johnson', 'michael.johnson@example.com'),
  (4, 'Emily', 'Davis', ''),
  (5, 'Robert', 'Wilson', ''),
  (6, 'Jennifer', 'Lee', 'jennifer.lee@example.com'),
  (7, 'Michael', 'Johnson', ''),
  (8, 'David', 'Brown', 'david.brown@example.com'),
  (9, 'Sarah', 'Miller', 'sarah.miller@example.com'),
  (10, 'Emily', 'Davis', ''),
  (11, 'David', 'Ha3user2', ''),
  (12, 'François', 'Davis', ''),
  (13, 'Jürg', 'Näf', ''),
  (14, 'Peter!', 'Da{vis$', '');
