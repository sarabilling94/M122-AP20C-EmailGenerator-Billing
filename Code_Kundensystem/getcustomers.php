<?php

$data = json_decode(file_get_contents('php://input'), true);

$servername = htmlspecialchars($data["servername"]);
$username = htmlspecialchars($data["username"]);
$password = htmlspecialchars($data["password"]);
$database = htmlspecialchars($data["database"]);

try {
    $db = new mysqli($servername, $username, $password, $database);
    $db->set_charset("utf8");

    if (mysqli_connect_errno()) {
        http_response_code(500);
        echo json_encode(array("message" => "Connection to database failed."));
    } else {
        $query = "SELECT * FROM tbl_customer";
        $result = $db->query($query);

        if ($result) {
            $data = array();

            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }

            http_response_code(200);
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
        } else {
            http_response_code(500);
            echo json_encode(array("message" => "Error occurred while fetching records."));
        }
    }
} catch (Exception $e) {
    http_response_code(500);
    echo json_encode(array("message" => "Connection to database failed."));
}
