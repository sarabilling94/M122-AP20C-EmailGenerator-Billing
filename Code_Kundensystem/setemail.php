<?php

$data = json_decode(file_get_contents('php://input'), true);

$servername = htmlspecialchars($data["servername"]);
$username = htmlspecialchars($data["username"]);
$password = htmlspecialchars($data["password"]);
$database = htmlspecialchars($data["database"]);
$customers = $data["customers"];

if (!empty($servername) && !empty($username) && !empty($password) && !empty($database) && !empty($customers)) {
    try {
        $db = new mysqli($servername, $username, $password, $database);

        if (mysqli_connect_errno()) {
            http_response_code(500);
            echo json_encode(array("message" => "Connection to database failed."));
        } else {
            foreach ($customers as $customer) {
                $idCustomer = $customer["id_customer"];
                $email = $customer["email"];
            
                $query = "UPDATE tbl_customer SET email = ? WHERE id_customer = ?";
                $stmt = $db->prepare($query);
                $stmt->bind_param("si", $email, $idCustomer);
            
                if (!$stmt->execute()) {
                    http_response_code(500);
                    echo json_encode(array("message" => "Error occurred while updating email for customer with ID $idCustomer."));
                    exit;
                }
            
                $stmt->close();
            }

            http_response_code(200);
            echo json_encode(array("message" => "Emails have been updated."));
        }
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(array("message" => "Connection to database failed."));
    }
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Servername, username, password, database, and customers array are required."));
}
