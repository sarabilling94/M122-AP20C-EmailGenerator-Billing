import configparser
import json
import requests

class DatabaseClient:
    def __init__(self, config_file):
        self.config = configparser.ConfigParser()
        self.config.read(config_file)
        self.servername = self.config.get("Database", "servername")
        self.username = self.config.get("Database", "username")
        self.password = self.config.get("Database", "password")
        self.database = self.config.get("Database", "database")
        self.urlget = self.config.get("Database", "urlget")
        self.urlpost = self.config.get("Database", "urlpost")

    def get_data(self):
        data = {
            "servername": self.servername,
            "username": self.username,
            "password": self.password,
            "database": self.database
        }
        json_data = json.dumps(data)

        headers = {
            "Content-Type": "application/json",
        }

        return requests.post(self.urlget, data=json_data, headers=headers, verify=True)

    def post_data(self, customers):
        jsonCustomers = {
            "servername": self.servername,
            "username": self.username,
            "password": self.password,
            "database": self.database,
            "customers": [
                {"id_customer": customer.id_customer, "email": customer.email}
                for customer in customers
            ]
        }

        return requests.post(self.urlpost, json=jsonCustomers, verify=True)