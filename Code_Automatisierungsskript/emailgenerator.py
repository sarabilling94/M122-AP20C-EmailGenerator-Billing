#!/usr/bin/python3
import logging
import datetime
from databaseclient import DatabaseClient
from customer import Customer, map_to_customer, generate_emails
import subprocess

# logging
logging.basicConfig(
    filename=f"logs/emailgenerator_{datetime.datetime.now().strftime('%Y-%m-%d')}.log",
    level=logging.INFO,
    format='%(asctime)s %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

logging.info("emailgenerator.py started.")
print("emailgenerator.py started.")

db_client = DatabaseClient("config.cfg")

# get customer data
logging.info(f"Fetching customer data from {db_client.urlget}.")
print(f"Fetching customer data from {db_client.urlget}.")
response = db_client.get_data()

if response.status_code == 200:
    logging.info("Response code 200.")
    print("Response code 200.")
    customers = []

    # map data to customer class
    try:
        logging.info("Trying to read and map json data.")
        print("Trying to read and map json data.")
        json_response = response.json()
        # map to Customer class
        customers = map_to_customer(json_response)
    except Exception as e:
        print(f"An error occurred while reading the response json: {str(e)}")
        logging.error(f"An error occurred while reading the response json: {str(e)}")
        exit()

    # generate emails for customers without email
    customers_with_no_email = [customer for customer in customers if not customer.email]

    if len(customers_with_no_email) == 0:
        print("Found no customers without an email address.")
        logging.info("Found no customers without an email address.")
        exit()

    # create string with all customers without email addresses
    full_names = [f"{customer.first_name} {customer.last_name}" for customer in customers]
    full_names_string = ", ".join(full_names)

    print(f"Generating email addresses for: {full_names_string}")
    logging.info(f"Generating email addresses for: {full_names_string}")

    # clean input and generate email addresses
    modified_customers = generate_emails(customers_with_no_email)

    # create string with all new email addresses
    new_emails = [customer.email for customer in modified_customers]
    new_emails_string = ", ".join(new_emails)

    logging.info(f"New email addresses have been generated: {new_emails_string}")
    print(f"New email addresses have been generated: {new_emails_string}")

    # send modified customer data back
    logging.info(f"Sending modified customer data to {db_client.urlpost}")
    print(f"Sending modified customer data to {db_client.urlpost}")
    responseSetEmail = db_client.post_data(modified_customers)

    if responseSetEmail.status_code == 200:
        logging.info("New email addresses have been saved.")
        print("New email addresses have been saved.")

        # send email to user ubuntu with string of all new e-mail addresses
        subprocess.run(["bash", "sendemail.sh"], check=True, input=f"New e-mail addresses generated\n{new_emails_string}\n", text=True)

    else:
        logging.error("An error occurred. The email addresses could not be saved.")
        print("An error occurred. The email addresses could not be saved.")

        # send email to user ubuntu with error message
        subprocess.run(["bash", "sendemail.sh"], check=True, input=f"E-mail generator error\nError occurred while saving new e-mail addresses: {new_emails_string}\n", text=True)

else:
    print("Error occurred while retrieving customer data.")
    logging.error("Error occurred while retrieving customer data.")

    # send email to user ubuntu with error message
    subprocess.run(["bash", "sendemail.sh"], check=True, input=f"E-mail generator error\nError occurred while fetching customer data.", text=True)