class Customer:
    def __init__(self, id_customer, first_name, last_name, email):
        self.id_customer = id_customer
        self.first_name = first_name
        self.last_name = last_name
        self.email = email

def map_to_customer(json):
    customers = []
    for customer in json:
        id_customer = customer["id_customer"]
        first_name = customer["firstName"]
        last_name = customer["lastName"]
        email = customer["email"]
        customers.append(Customer(id_customer, first_name, last_name, email))

    if len(customers) > 0:
        return customers
    else:
        raise ValueError("Found no customer data in json.")

def generate_emails(customers):
    for customer in customers:
        cleaned_first_name = clean_name(customer.first_name)
        cleaned_last_name = clean_name(customer.last_name)
        generated_email = f"{cleaned_first_name}.{cleaned_last_name}@enterprise.ch"

        emailTaken = any(c.email == generated_email for c in customers) or any(c.email == generated_email for c in customers)

        if emailTaken:
            customer.email = f"{customer.first_name.lower()}.{customer.last_name.lower()}_{customer.id_customer}@enterprise.ch"
        else:
            customer.email = generated_email
    return customers

def clean_name(name):
    valid_chars = set("abcdefghijklmnopqrstuvwxyz")

    replacements = {
        "ä": "ae",
        "ü": "ue",
        "ö": "oe",
        "è": "e",
        "é": "e",
        "à": "a",
        "ñ": "n",
        "ç": "c"
    }

    cleaned_name = ""
    for char in name:
        char_lower_case = char.lower()
        if char_lower_case in valid_chars:
            cleaned_name += char_lower_case
        elif char_lower_case in replacements:
            cleaned_name += replacements[char_lower_case]
    
    return cleaned_name